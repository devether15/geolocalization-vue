 import firebase from 'firebase'
 import firestore from 'firebase/firestore'
 // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyCencJcgXU8IeFplnkdwgLw_oGm_M0YmE8",
    authDomain: "udemy-geo-ninjas-3fce2.firebaseapp.com",
    databaseURL: "https://udemy-geo-ninjas-3fce2.firebaseio.com",
    projectId: "udemy-geo-ninjas-3fce2",
    storageBucket: "udemy-geo-ninjas-3fce2.appspot.com",
    messagingSenderId: "16398157906",
    appId: "1:16398157906:web:95cf958b50f026cd81b78b",
    measurementId: "G-84H71LVJ6L"
  };
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);
  export default firebaseApp.firestore()
  